/* S-45 ACTIVITY:
1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
 - The course name should be in the card title.
 - The description and price should be in the card body.
*/

/*import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard(){
	return(
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h5>Sample Course</h5>
						</Card.Title>
							<Card.Text>
								<h6>Description:</h6>
							</Card.Text>
							
							<Card.Text>
								This is a sample course offering
							</Card.Text>
							
							<Card.Text className="mb-0">
								Price:
							</Card.Text>
							
							<Card.Text className="mt-0">
								PhP 40,000
							</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}*/

// S-45 ACTIVITY SOLUTION + S-46 discussion codes
import { useState } from 'react';
// Proptypes - used to validate props
import PropTypes from 'prop-types'
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
	// console.log(props);
	const {name, description, price} = courseProp;

	// State hook - used to keep track of information related to individual components
	// Syntax: const [getter, setter] = useState(initialGetterValue);
	const [count, setCount] = useState(0);
	const [seats, seatCount] = useState(30);

/* ACTIVITY S-46:
Create a seats state in the CourseCard component and set the initial value to 30.
For every enrollment, deduct one to the seats.
If the seats reaches zero do the following:
  - Do not add to the count.
  - Do not deduct to the seats.
  - Show an alert that says No more seats available.
*/
	function enroll(){
		setCount(count + 1);
		seatCount(seats - 1)
		if(seats === 0){
			setCount(count);
			seatCount(seats);
			alert("No more seats available!");
		}
	}

  return (
    <Card className="mb-3">
      <Card.Body >
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>
        <Button variant="primary" onClick={enroll}>Enroll</Button>
      </Card.Body>
    </Card>
  )
}

// Checks the validity of the PropTypes
CourseCard.propTypes = {
	// "shape" method is used to check if a prop object conforms to a specific shape
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}