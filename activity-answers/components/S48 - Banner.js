// import Button from 'react-bootstrap/Button';
// Bootstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Column from 'react-bootstrap/Column';
// import { Row } from 'react-bootstrap';
// import { Column } from 'react-bootstrap';
import { Fragment } from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import NotFound from '../components/NotFound';

export default function Banner(){
	const [page, setPage] = useState('');

	return (
		{(page === "/") ?
			<Row>
				<Col className="p-5">
					<h1>Zuitt Coding Program</h1>
					<p>Opportunities for everyone, everywhere</p>
					<Button as={Link} to="/courses" variant="primary">Enroll Now!</Button>
				</Col>
			</Row>
			:
			<Fragment>
				<NotFound />
			</Fragment>
		}
	)
}