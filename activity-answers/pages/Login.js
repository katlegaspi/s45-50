import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login(){
  const [email, setEmail] = useState('');
  const [password, setpassword] = useState('');
  
  const [isActive, setIsActive] = useState(false);

/* S47 ACTIVITY: 
- Create a Login page that simulates user login and authentication using an email and a password.
- When all the input fields are filled, enable the submit button.
- Upon clicking the submit button, a message will alert the user of a successful login.
*/
  function login(e){
    e.preventDefault();
    setEmail('');
    setpassword('');

    alert('User login successful.')
  }
  useEffect(() => {
    if (email !== '' && password !== '')
    {
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [email, password])

  return (
    <Form onSubmit={(e) => login(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange = { e => setEmail(e.target.value) }
          required
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange = { e => setpassword (e.target.value) }
          required />
      </Form.Group>

      { isActive ? 
        <Button variant="success" type="submit" id="submitBtn">
          Login
        </Button>
        :
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Login
        </Button>
      }
    </Form>
  )
}