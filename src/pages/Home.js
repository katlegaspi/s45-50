import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// S-45 ACTIVITY: Render the CourseCard component in the Home page.*/
// import CourseCard from '../components/CourseCard';

export default function Home(){
	const data={
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll now!"
	}
	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	);
}